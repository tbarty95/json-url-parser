import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import org.json.JSONObject;
import org.json.JSONException;

public class JsonParser {

	public static final double knotValue = 0.868976;
	
	private static String readAll(Reader rd) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rd.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	  }
	
	public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
	    InputStream is = new URL(url).openStream();
	    try {
	      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
	      String jsonText = readAll(rd);
	      JSONObject json = new JSONObject(jsonText);
	      return json;
	    } finally {
	      is.close();
	    }
	  }
	
	public static void main(String[] args) throws IOException, JSONException {

		//URL weather = new URL("http://api.wunderground.com/api/5d471eae9c9fc3eb/conditions/q/pws:IENGLAND369.json");
		JSONObject json = readJsonFromUrl("http://api.wunderground.com/api/5d471eae9c9fc3eb/conditions/q/pws:IENGLAND369.json");
	    JSONObject observation = (JSONObject) json.get("current_observation");
	    double windSpeedMph = (double) observation.get("wind_mph");
	    double windSpeedKnots = windSpeedMph * knotValue;
	    System.out.println("Wind Speed (mph): " + windSpeedMph);
	    System.out.println("Wind Speed (Knots): " + windSpeedKnots);
	}
	
}
